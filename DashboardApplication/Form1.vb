﻿Imports System.Net.Sockets
Imports System.Threading
Imports System.IO
Imports System.Net
Imports Microsoft.Office.Interop

Public Class Form1
    'file path information
    Dim dateforname
    Dim filename As String
    Dim filepath As String
    Dim BookNumber As Integer
    Dim presentationNumber As Integer
    Dim SheetPresent As Boolean
    'Excel declarations
    Dim xlapp As Excel.Application
    ''''''''''''Archive declarations
    Dim ArchiveBook As Excel.Workbook
    Dim ArchiveBreadAndButter As Excel.Worksheet
    Dim ArchiveNegatives As Excel.Worksheet
    Dim archiveSalesOrders As Excel.Worksheet
    ''''''''''''Negatives declarations

    Dim NegativesDataSheet As Excel.Worksheet
    Dim NegativesChartSheet As Excel.Worksheet
    Dim NegativesChart
    '''''''''''''''Bread and Butter declarations
    Dim BreadAndButterBook As Excel.Workbook
    Dim BreadAndButterDataSheet As Excel.Worksheet
    Dim BreadAndButterSalesOrders As Excel.Worksheet
    Dim SalesOrdersRange As Excel.Range
    '''''''''''''''''''''Assignments declarations
    Dim AssignmentsSheet As Excel.Worksheet
    Dim AssignmentsData As Excel.Range
    ''''''''''''''''''''KPI Declarations
    Dim OnTimeDeliverySheet As Excel.Worksheet
    Dim InternalYieldSheet As Excel.Worksheet
    Dim internalYieldChart
    Dim OntimeDeliveryChart
    Dim KPIbook As Excel.Workbook
    'Dim InternalYieldSheet As Excel.Worksheet
    Dim OTIFSheet As Excel.Worksheet
    'PowerPoint declarations
    Dim PowerPointApp As PowerPoint.Application
    Dim PPPresentation As PowerPoint.Presentation
    Dim PresentationOpen As Boolean
    Dim ShortageSlide As PowerPoint.Slide
    Dim AssignmentSlide As PowerPoint.Slide
    Dim KPISlide As PowerPoint.Slide
    Dim BreadAndButterSlide As PowerPoint.Slide
    Dim NegativesSlide As PowerPoint.Slide
    Dim ShortageChart
    Dim OntimDeliveryChartPP As Object
    Dim InternalYeildChartPP As Object
    Dim BreadAndButterChartPP
    Dim NegativesChartPP

    ''Stuff for talking over the network
    Dim Listener As New TcpListener(65535)
    Dim Client As New TcpClient
    Dim messageRecieve As String = ""
    Dim messagenumber
    Dim IPAddressRecieve
    Dim t1 As Threading.Thread
    Dim myip As String

    Sub findmyIP()

        Dim myHost As String = Dns.GetHostName
        Dim ipEntry As IPHostEntry = Dns.GetHostEntry(myHost)

        For Each tmpIpAddress As IPAddress In ipEntry.AddressList
            If tmpIpAddress.AddressFamily = Sockets.AddressFamily.InterNetwork Then
                Dim ipAddress As String = tmpIpAddress.ToString
                myip = ipAddress
                Exit For
            End If
        Next
        If myip = "" Then
            Throw New Exception("No 10. IP found!")
        End If
    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)
        ArchiveUpdate()

    End Sub

    Sub ArchiveUpdate()
        'find the name and filepath of the shortage report taken today
        Try
            ShortageRptName()

            'Opens workbook then waits .4 sec. for file to open
            'xlapp = CreateObject("Excel.Application")
            xlapp = GetObject(, "Excel.Application")
            If isbookopen(filename) = False Then
                ArchiveBook = xlapp.Workbooks.Open(filepath, , , , , , , , , , False)
            Else
                ArchiveBook = xlapp.Workbooks(BookNumber)
            End If

            System.Threading.Thread.Sleep(400)

            ''Checks if the Archive already has a Bread and Butter Report Or Negatives Report Sheet and adds any that are missing
            SetArchiveBreadandButter()
            SetArchiveNegatives()

            '''''Assignes Bread and Butter book and gets data
            If isbookopen("SemiAutoB&B.xlsm") = True Then
                BreadAndButterBook = xlapp.Workbooks(BookNumber)
                BreadAndButterDataSheet = BreadAndButterBook.Sheets("BREAD AND BUTTER")
                BreadAndButterSalesOrders = BreadAndButterBook.Sheets("SALES ORDERS")
                'COPY BREAD AND BUTTER DATA 
                BreadAndButterDataSheet.Activate()
                BreadAndButterDataSheet.Cells.Select()
                BreadAndButterDataSheet.Cells.Copy()

                'Pastes Bread and Butter to B&B archive and clears clipboard
                ArchiveBreadAndButter.Activate()
                ArchiveBreadAndButter.Paste()
                Clipboard.Clear()

                'Runs subroutine that copies Mfg. Parts-By Ship Date worksheet to Bread And Butter workbook
                salesOrdersCopy()


                ''''''assignes negatives book and gets data 
                NegativesDataSheet = BreadAndButterBook.Sheets("Negatives")
                NegativesChartSheet = BreadAndButterBook.Sheets("Chart")

                'Copies Negatives Data sheet
                NegativesDataSheet.Activate()
                NegativesDataSheet.Cells.Select()
                NegativesDataSheet.Cells.Copy()

                'Pastes Negatives Data to Negatives Archive sheet then clears the clipboard
                ArchiveNegatives.Activate()
                ArchiveNegatives.Cells(1, 1).select()
                ArchiveNegatives.Paste()
                Clipboard.Clear()

                'Adds Negatives chart to Negatives Archive
                NegativesChartSheet.Activate()
                NegativesChartSheet.ChartObjects("Chart 3").copy()
                ArchiveNegatives.Paste()
                Clipboard.Clear()
                ShortageDisplay()
                System.Threading.Thread.Sleep(400)
                ArchiveBook.Close(True)
            Else
                MsgBox("File not found")
            End If

        Catch ex As Exception  ''if there is an error is is recorded to a text file along with the date
            Dim message As String = Today.ToString + " " + "Error: " + ex.Message
            recordError(message)
        End Try

        If isbookopen(filename) = True Then
            ArchiveBook.Close(true)
        End If
    End Sub
    Sub salesOrdersCopy()

        'Copies Sales Orders
        archiveSalesOrders = ArchiveBook.Sheets("Mfg. Parts -By Ship Date")
        archiveSalesOrders.Activate()
        archiveSalesOrders.Cells.Copy()

        'Pastes Salses orders info to bread and butter book
        BreadAndButterSalesOrders.Cells.PasteSpecial(Excel.XlPasteType.xlPasteAll)
        Clipboard.Clear()

    End Sub
    Sub ShortageRptName()
        dateforname = DateTime.Now.Month.ToString + "_" + DateTime.Now.Day.ToString + "_" + DateTime.Now.Year.ToString
        filename = "FGC_Shortage_Report_for_" + dateforname + ".xlsx"
        filepath = "H:\DATA\MFG\Clear-Com\StatusBoard\FGC Shortage Report Archive\" + filename
    End Sub


    Function PPPresentationOpen(NameofPresentation As String) As Boolean
        Dim open As Boolean
        PowerPointApp = CreateObject("PowerPoint.Application")
        For presentationNumber = PowerPointApp.Presentations.Count To 1 Step -1
            If PowerPointApp.Presentations(presentationNumber).Name = NameofPresentation Then Exit For
        Next presentationNumber
        open = False
        If presentationNumber <> 0 Then
            open = True
            Return True
        Else
            Return False
        End If
    End Function

    Function isbookopen(strname As String) As Boolean
        Dim notopen As Boolean
        On Error Resume Next
        'gets excel 
        xlapp = GetObject(, "Excel.Application")

        'Finds book being looked for (strname) 
        For BookNumber = xlapp.Workbooks.Count To 1 Step -1
            If xlapp.Workbooks(BookNumber).Name = strname Then Exit For
        Next BookNumber
        'Sets variable to false to avoid false positives
        isbookopen = False
        'Checks if file is open (if booknumber isn't zero file is open already)
        If BookNumber <> 0 Then
            isbookopen = True
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Form1_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Timer1.Stop()
            Listener.Stop()
            t1.Abort()
        Catch ex As Exception
        End Try

    End Sub


    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()
        Dim ListThread As New Thread(New ThreadStart(AddressOf Listening))     'creates the thread
        ListThread.Start()      'Starts the thread
        findmyIP()
        lblVersion.Text = Application.ProductVersion
        Me.Text = myip
    End Sub
    Function IsPowerPointRunning()
        'CreateObject("Shell.Application").ShellExecute("powerpnt.exe")
        PowerPointApp = CreateObject("PowerPoint.Application")
        If PowerPointApp Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub Listening()
        Listener.Start()
    End Sub

    Sub FormatPowerPoint()
        If IsPowerPointRunning() = False Then
            PowerPointApp = New PowerPoint.Application
        End If
        If PPPresentationOpen("PresentationForTV1 - Copy.pptm") = False Then   '''''''''''''''''''''''''''''''''''''''''''''''''REMOVE -COPY BEFORE RELEASING!!!!!!!!!!!!!!!!!
            PPPresentation = PowerPointApp.Presentations.Open("H:\DATA\MFG\Clear-Com\StatusBoard\Do Not Open\PresentationForTV1 - Copy.pptm")
        Else
            PPPresentation = PowerPointApp.Presentations(presentationNumber)
        End If

    End Sub
    Sub ShortageReportSlide()
        Try
            ShortageSlide = PPPresentation.Slides(1)
            'copies information to be sent to power point 
            BreadAndButterSalesOrders.Activate()
            SalesOrdersRange = BreadAndButterBook.Sheets("SalesOrdersForTV").Range("A1:F34")
            SalesOrdersRange.Font.Size = 12
            SalesOrdersRange.Font.Bold = True
            SalesOrdersRange.Copy()

            'Pastes Table to slide and positions and sizes chart
            Try
                ShortageChart = ShortageSlide.Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteOLEObject, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
                With ShortageSlide.Shapes(ShortageChart)
                    .IncrementLeft(-50)
                    .IncrementTop(-110)
                    .LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse
                    .Height = 540
                    .Width = 600
                End With
            Catch
            End Try


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs)
        FormatPowerPoint()
        ShortageReportSlide()
    End Sub
    '2014-09-15 ATN: Added Daily Assignments
    Sub AssignmentsDataSlide()
SecondSlide:
        Try
            AssignmentSlide = PPPresentation.Slides(2)
            'Opens Production Supervisors To Do List spreadsheet and copies it to power point slide
            AssignmentsSheet = BreadAndButterBook.Sheets("Daily Work Assignments")
            AssignmentsSheet.Activate()
            AssignmentsSheet.Select()
            AssignmentsSheet.Range("A1", "G38").Select()
            AssignmentsSheet.Range("A1", "G38").Font.Size = 24 '2014-10-15 ATN: Changed Font Size from 8 to 16
            AssignmentsSheet.Range("A1", "G38").Copy()
            Dim todolist

            'adds table to power point and links the data for updates
            With AssignmentSlide
                todolist = .Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteOLEObject, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
                .Shapes(todolist).IncrementLeft(-65)
                .Shapes(todolist).IncrementTop(-80)
                .Shapes(todolist).Height = 775
                .Shapes(todolist).Width = 600
            End With
            Clipboard.Clear()

        Catch ex As Exception
            MsgBox(ex.Message)
            'Dim message As String = Today.ToString + " " + "Error: " + ex.Message
            'recordError(message)
        End Try

    End Sub

    Sub KPIDataSlide()
        KPISlide = PPPresentation.Slides(3)
        'Assignes values to all variables
        InternalYieldSheet = BreadAndButterBook.Sheets("KPI Information")

        internalYieldChart = InternalYieldSheet.ChartObjects(1)
        OntimeDeliveryChart = InternalYieldSheet.ChartObjects(2)

        'copies internal yield chart to Presentation
        internalYieldChart.Activate()
        internalYieldChart.Copy()
        System.Threading.Thread.Sleep(300)

        With KPISlide
            InternalYeildChartPP = .Shapes.Paste.Name
            .Shapes(InternalYeildChartPP).IncrementLeft(-50)   'changed from 92
            .Shapes(InternalYeildChartPP).IncrementTop(-130)
            .Shapes(InternalYeildChartPP).LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse
            .Shapes(InternalYeildChartPP).Height = 275
            .Shapes(InternalYeildChartPP).Width = 450
        End With
        Clipboard.Clear()

        'copies on time delivery chart to presentation
        OntimeDeliveryChart.Activate()
        OntimeDeliveryChart.Copy()
        With KPISlide
            OntimDeliveryChartPP = .Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteShape, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
            .Shapes(OntimDeliveryChartPP).IncrementLeft(-55)   'changed from 92
            .Shapes(OntimDeliveryChartPP).IncrementTop(148)   'changed from 250
            .Shapes(OntimDeliveryChartPP).LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse
            .Shapes(OntimDeliveryChartPP).Height = 270
            .Shapes(OntimDeliveryChartPP).Width = 450
        End With
        Clipboard.Clear()
        ' KPIbook.Close(False)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs)
        AssignmentsDataSlide()
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs)
        KPIDataSlide()
    End Sub

    Sub BreadAndButterDataSlide()

        BreadAndButterSlide = PPPresentation.Slides(4)

        BreadAndButterDataSheet.Activate()
        BreadAndButterDataSheet.Range("A1", "G25").Copy()

        With BreadAndButterSlide
            Try
                BreadAndButterChartPP = .Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteOLEObject, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
                .Shapes(BreadAndButterChartPP).IncrementLeft(-40)
                .Shapes(BreadAndButterChartPP).IncrementTop(-30)
                .Shapes(BreadAndButterChartPP).Height = 775
                .Shapes(BreadAndButterChartPP).Width = 600
            Catch
            End Try

        End With
        Clipboard.Clear()
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs)
        BreadAndButterDataSlide()
    End Sub

    Sub NegativesChartSlide()
        NegativesSlide = PPPresentation.Slides(5)

        NegativesChartSheet = BreadAndButterBook.Sheets("Chart")
        NegativesChart = NegativesChartSheet.ChartObjects(1)

        NegativesChart.activate()
        NegativesChart.copy()
        System.Threading.Thread.Sleep(300)
        With NegativesSlide
            Try
                NegativesChartPP = .Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteOLEObject, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
                .Shapes(NegativesChartPP).LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse
                .Shapes(NegativesChartPP).Width = 500
                .Shapes(NegativesChartPP).Height = 400
            Catch
            End Try
        End With
        Clipboard.Clear()



    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs)
        NegativesChartSlide()
    End Sub

    Private Sub Button8_Click(sender As System.Object, e As System.EventArgs)
        deleteShapes(1)
    End Sub
    Sub deleteShapes(ByVal SlideNumber As Integer)
        FormatPowerPoint()
        Dim shapes As Integer
        If PPPresentation.Slides.Count > 1 Then
            For shapes = PPPresentation.Slides(SlideNumber).Shapes.Count To 2 Step -1
                PPPresentation.Slides(SlideNumber).Shapes(shapes).Delete()
            Next
        End If
    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs)
        deleteShapes(3)
    End Sub

    Private Sub Button9_Click(sender As System.Object, e As System.EventArgs) Handles Button9.Click
        dailyupdate()
    End Sub
    Sub dailyupdate()
        'System.Threading.Thread.Sleep(60000)
        ArchiveUpdate()
        PowerPointApp = CreateObject("PowerPoint.Application")
        PowerPointApp.ActivePresentation.SlideShowWindow.Activate()
    End Sub
    Private Sub Button10_Click(sender As System.Object, e As System.EventArgs) Handles Button10.Click
        ArchiveUpdate()

        deleteShapes(1)
        deleteShapes(2)
        deleteShapes(3)
        deleteShapes(4)
        deleteShapes(5)

        ShortageReportSlide()
        'AssignmentsDataSlide()
        'kpiSlideSecondTry()
        BreadAndButterDataSlide()
        NegativesChartSlide()
        'PPPresentation.Slides(2).Delete()
        'PPPresentation.Slides(2).Delete()

        ' PPPresentation.SlideShowSettings.Run()
    End Sub

    Private Sub Button11_Click(sender As System.Object, e As System.EventArgs)
        ShortageRptName()

        xlapp = CreateObject("Excel.Application")

        If isbookopen(filename) = False Then
            ArchiveBook = xlapp.Workbooks.Open(filepath, , , , , , , , , , False)
        Else
            ArchiveBook = xlapp.Workbooks(BookNumber)
        End If


        Dim ArchiveSheetNumber As Integer
        For ArchiveSheetNumber = ArchiveBook.Worksheets.Count To 1 Step -1
            If ArchiveBook.Worksheets(ArchiveSheetNumber).Name.ToString = "Negatives" Then
                MsgBox("true")
            ElseIf ArchiveBook.Worksheets(ArchiveSheetNumber).Name.ToString = "Negatives" Then
            End If
        Next
    End Sub

    Sub IsSheetNameArchive(name As String)
        'xlapp = CreateObject("Excel.Application")
        xlapp = GetObject(, "Excel.Application")
        ShortageRptName()
        SheetPresent = False
        ' If isbookopen(filename) = True Then
        '  BreadAndButterBook = xlapp.Workbooks(BookNumber)
        Dim sheetnumber As Integer
        For sheetnumber = ArchiveBook.Sheets.Count To 1 Step -1
            If ArchiveBook.Sheets(sheetnumber).Name.ToString = name Then
                SheetPresent = True
            Else
                SheetPresent = False
            End If
        Next

    End Sub

    Sub SetArchiveBreadandButter()
        'xlapp = CreateObject("Excel.Application")
        xlapp = GetObject(, "Excel.Application")
        ShortageRptName()
        If isbookopen(filename) = False Then
            ArchiveBook = xlapp.Workbooks.Open(filepath, , False, , , , , , , True, False)
        Else
            ArchiveBook = xlapp.Workbooks(BookNumber)
        End If
        Dim sheetnumber As Integer
        For sheetnumber = ArchiveBook.Sheets.Count To 1 Step -1
            'MsgBox(ArchiveBook.Sheets(sheetnumber).name)
            If ArchiveBook.Sheets(sheetnumber).name = "Bread and Butter Report" Then
                'MsgBox("true")
                ArchiveBreadAndButter = ArchiveBook.Sheets(sheetnumber)
                'MsgBox(ArchiveBreadAndButter.Name)
                GoTo BreadAndButterOpen
            Else
                ' MsgBox("false")
            End If
        Next
        ArchiveBreadAndButter = ArchiveBook.Sheets.Add
        ArchiveBreadAndButter.Name = "Bread and Butter Report"
BreadAndButterOpen:

    End Sub
    Sub SetArchiveNegatives()
        'xlapp = CreateObject("Excel.Application")
        xlapp = GetObject(, "Excel.Application")
        ShortageRptName()
        If isbookopen(filename) = False Then
            ArchiveBook = xlapp.Workbooks.Open(filepath, , False, , , , , , , True, False)
        Else
            ArchiveBook = xlapp.Workbooks(BookNumber)
        End If
        Dim sheetnumber As Integer
        For sheetnumber = ArchiveBook.Sheets.Count To 1 Step -1
            'MsgBox(ArchiveBook.Sheets(sheetnumber).name)
            If ArchiveBook.Sheets(sheetnumber).name = "Negatives" Then
                'MsgBox("true")
                ArchiveNegatives = ArchiveBook.Sheets(sheetnumber)
                'MsgBox(ArchiveBreadAndButter.Name)
                GoTo NegativesOpen
            Else
                ' MsgBox("false")
            End If
        Next
        ArchiveNegatives = ArchiveBook.Sheets.Add
        ArchiveNegatives.Name = "Negatives"
NegativesOpen:

    End Sub

    Private Sub Button13_Click(sender As System.Object, e As System.EventArgs)
        SetArchiveBreadandButter()
        SetArchiveNegatives()
        xlapp.ScreenUpdating = True
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick

        If Listener.Pending = True Then
            messageRecieve = ""
            Client = Listener.AcceptTcpClient()

            Dim Reader As New StreamReader(Client.GetStream())
            While Reader.Peek > -1
                messageRecieve = messageRecieve + Convert.ToChar(Reader.Read()).ToString
            End While

            Dim s As String = messageRecieve
            Dim words As String() = s.Split(New Char() {","c})
            messagenumber = words(1)
            Select Case messagenumber
                Case 1
                    lblStatus.Text = "Master Program Working"
                Case 3
                    dailyupdate()
                    lblStatus.Text = "Master Program done for " + Date.Today
                Case 5
                    '  MessageSend = "6"
                Case 7
                    'UpdaterThread()
            End Select
            '   SendMessage()

        End If
        ''lblIP.Text = txt0.Text
        ''My.Settings.targetIP = txt0.Text
        ''My.Settings.Save()
    End Sub


    Sub powerpointstop()
        PowerPointApp = CreateObject("PowerPoint.Application")
        PPPresentation = PowerPointApp.ActivePresentation

        ' PowerPointApp.ActivePresentation.SlideShowSettings.
    End Sub

    Private Sub Button4_Click_1(sender As System.Object, e As System.EventArgs)
        ArchiveUpdate()
        ShortageReportSlide()
    End Sub

    Private Sub Button3_Click_1(sender As System.Object, e As System.EventArgs)
        AssignmentsDataSlide()
    End Sub

    Private Sub Button2_Click_1(sender As System.Object, e As System.EventArgs)
        KPIDataSlide()
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs)
        BreadAndButterDataSlide()
    End Sub

    Private Sub Button5_Click_1(sender As System.Object, e As System.EventArgs)
        NegativesChartSlide()
    End Sub
    Sub recordError(ByVal Error1 As String)
        Try
            Using sw As New IO.StreamWriter("H:\DATA\MFG\Clear-Com\StatusBoard\Presentation Errors\Errors.txt", True)
                sw.WriteLine(Error1)
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Sub ShortageDisplay()
        BreadAndButterSalesOrders.Cells.Copy()
        Dim soForTV As Excel.Worksheet
        soForTV = BreadAndButterBook.Sheets("SalesOrdersForTV")
        soForTV.Activate()
        soForTV.Cells.PasteSpecial()
        Clipboard.Clear()
        With soForTV
            .Range("A1").EntireColumn.Delete()
            .Range("F1").EntireColumn.Delete()
            .Range("F1").EntireColumn.Delete()
            .Range("F1").EntireColumn.Delete()
            .Range("F1").EntireColumn.Delete()
        End With
    End Sub

    Private Sub Button1_Click_2(sender As System.Object, e As System.EventArgs)
        ShortageDisplay()
    End Sub

    Private Sub Button1_Click_3(sender As System.Object, e As System.EventArgs)
        ArchiveUpdate()
    End Sub

    Sub kpiSlideSecondTry()
        KPIbook = xlapp.Workbooks.Open("H:\DATA\MFG\Clear-Com\StatusBoard\KPIs - OTIF-Yield-DOA.xlsx", , , , , , , , , , False)
        InternalYieldSheet = KPIbook.Sheets("Internal Yield")
        OTIFSheet = KPIbook.Sheets("OTIF")

        internalYieldChart = InternalYieldSheet.ChartObjects(1)
        OntimeDeliveryChart = OTIFSheet.ChartObjects(1)
        KPISlide = PPPresentation.Slides(3)

        internalYieldChart.activate()
        internalYieldChart.copy()

        Dim powerpointinternalchart
        With KPISlide
            powerpointinternalchart = .Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteOLEObject, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
            .Shapes(powerpointinternalchart).IncrementLeft(-55)
            .Shapes(powerpointinternalchart).IncrementTop(-125)
            .Shapes(powerpointinternalchart).LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse
            .Shapes(powerpointinternalchart).Height = 275
            .Shapes(powerpointinternalchart).Width = 450
        End With
        Clipboard.Clear()

        'copies on time delivery chart to presentation
        OntimeDeliveryChart.activate()
        OntimeDeliveryChart.Copy()
        With KPISlide
            OntimDeliveryChartPP = .Shapes.PasteSpecial(PowerPoint.PpPasteDataType.ppPasteOLEObject, , , , , Microsoft.Office.Core.MsoTriState.msoTrue).Name
            .Shapes(OntimDeliveryChartPP).IncrementLeft(-48)
            .Shapes(OntimDeliveryChartPP).IncrementTop(140)
            .Shapes(OntimDeliveryChartPP).LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoFalse
            .Shapes(OntimDeliveryChartPP).Height = 270
            .Shapes(OntimDeliveryChartPP).Width = 450
        End With
        KPIbook.Close()
    End Sub

    Private Sub Button2_Click_2(sender As System.Object, e As System.EventArgs)

    End Sub
End Class
